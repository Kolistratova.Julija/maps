import React from "react";
import {Link} from "react-router-dom";

export function MainPage(){
    return(
        <nav>

            <ol>
                <li>
                    <Link className="" to="/test1">TEST1</Link>
                </li>
                <li>
                    <Link className="" to="/test2">TEST2</Link>
                </li>
                <li>
                    <Link className="" to="/test3">TEST3</Link>
                </li>
                <li>
                    <Link className="" to="/test4">TEST4</Link>
                </li>
            </ol>
        </nav>
    );
}