import React from 'react';
import './App.css';
import {Route, Routes} from "react-router-dom";
import {Test1} from "./tests/Test1"
import {Test2} from "./tests/Test2";
import {Test3} from "./tests/Test3";
import {Test4} from "./tests/Test4";
import {MainPage} from "./tests/MainPage"


function App() {
  return (
      <>
          <Routes>
              <Route path="/" element={<MainPage/>}/>
              <Route path="/test1" element={<Test1/>}/>
              <Route path="/test2" element={<Test2/>}/>
              <Route path="/test3" element={<Test3/>}/>
              <Route path="/test4" element={<Test4/>}/>
          </Routes>
      </>
  );
}

export default App;
